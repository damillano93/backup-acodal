
const fs = require('fs');
const { promisify } = require('util');
const cron = require('node-cron');
const dateFormat = require('dateformat');
const nodemailer = require('nodemailer');

const removeFile = promisify(fs.unlink)

const exec = promisify(require('child_process').exec);
const { USER_MAIL: userMail,
    PASSWORD_MAIL: passwordMail,
    MONGO_USER: mongoUser,
    MONGO_PASSWORD: mongoPassword,
    MONGO_HOST: mongoHost,
    MONGO_DB_NAME: mongoDbName } = process.env
const mails = [ 'soporte@acodal.org.co', 'gestionafiliados@acodal.org.co' ] 
const sendMail = async (to, subject, body, attachment) => {
    console.log('-- SEND MAIL TASK --');
    const mailOptions = {
        from: process.env.NOTIFIER_MAIL,
        to: to,
        subject: subject,
        html: body,
        attachments: [{
            filename: attachment[0],
            path: attachment[0],
            cid: attachment[0] // same cid value as in the html img src
        }]
    }
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: userMail,
            pass: passwordMail
        }
    })

    await transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            console.error('Error trying to send email', err)
        } else {
            console.log(info)
        }
    })
};

const executeScript = async (body) => {
    console.log('-- EXECUTE SCRIPT TASK --')
    try {
        const { stdout, stderr } = await exec(body);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
    } catch (err) {
        console.error(err);
    };
};

const compressFile = async (backupDate) => {
    console.log(' -- COMPRESS FILE TASK -- ')
    try {
        const { stdout, stderr } = await exec(`zip -uq ${backupDate}.zip  /usr/src/app/data/${backupDate}/${mongoDbName}/*`);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
    } catch (err) {
        console.error(err);
    };
};

const removeFolder = async (path) => {
    console.log(' -- REMOVE FOLDER TASK -- ')
    try {
        const { stdout, stderr } = await exec(`rm -rf ${path}`);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
    } catch (err) {
        console.error(err);
    };
};

const executeBackup = async () => {
    const now = new Date();
    const backupDate = dateFormat(now, 'isoDate');
    //const body = `docker run --rm -i -v "$PWD":/data --link=mongo-afiliados:mongo --net aplicaciones leafney/alpine-mongo-tools:latest mongodump --uri mongodb://${mongoUser}:${mongoPassword}@${mongoHost}/${mongoDbName}?authSource=admin  --out /data/${backupDate}`
    const body = `mongodump --uri mongodb://${mongoUser}:${mongoPassword}@${mongoHost}/${mongoDbName}?authSource=admin  --out /usr/src/app/data/${backupDate}`
    await executeScript(body)
    await executeScript('ls $PWD')
    await delay(3000)
    await compressFile(backupDate)
    await sendMail(mails, `backup plataforma acodal ${backupDate}`, `adjunto envio el backup ${backupDate}`, [`/usr/src/app/${backupDate}.zip`])
    await delay(3000)
    await removeFile(`/usr/src/app/${backupDate}.zip`)
    await removeFolder(`/usr/src/app/data/${backupDate}`)
};
const delay = (freq) => new Promise(resolve => setTimeout(resolve, freq))

const backupTask = cron.schedule('0 1 * * *', async () => {
    console.log('-- BACKUP START --')
    await executeBackup()
    console.log('-- BACKUP FINISH --')
}, {
    scheduled: false,
    timezone: 'America/Bogota'
})


backupTask.start();